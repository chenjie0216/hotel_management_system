#include "hotel.h"

Hotel::Hotel(void)
{
	//读取配置文件
	fstream fr(HOTEL_PATH, ios::in | ios::binary);
	//读取不成功，手动设置
	if(!fr)
	{
		cout << "请配置酒店房间信息" << endl;
		cout << "请输入酒店的楼层数:";
		cin >> floor;
		room_cnt = new int[floor];
		rooms = new Room*[floor];

		for(int i=0; i<floor; i++)
		{
			cout << "请输入第" << i+1 << "层的房间数量:";
			cin >> room_cnt[i];
			rooms[i] = new Room[room_cnt[i]];
			for(int j=0; j<room_cnt[i]; j++)
			{
				rooms[i][j].set_id((i+1)*100+j+1);
				switch(i)
				{
					case 0: rooms[i][j].set_type(Type1); break;
					case 1: rooms[i][j].set_type(Type2); break;
					case 2: rooms[i][j].set_type(Type3); break;
					case 3: rooms[i][j].set_type(Type4); break;
				}
				cout << "请输入" << rooms[i][j].get_id() << "的价格:";
				float price = 0;
				cin >> price;
				rooms[i][j].set_price(price);
				cout << "请输入该房间的人数上限:";
				cin >> rooms[i][j].cnt;
				rooms[i][j].guests = NULL;
			}	
		}
		
		fstream fw(HOTEL_PATH, ios::out | ios::binary | ios::trunc);
		fw.write((char*)&floor, sizeof(floor));
		fw.write((char*)room_cnt, sizeof(int)*floor);
		for(int i=0; i<floor; i++)
		{
			fw.write((char*)rooms[i], sizeof(Room)*room_cnt[i]);
		}
		fw.close();
	}
	else
	{	
		fr.read((char*)&floor, sizeof(floor));
		room_cnt = new int[floor];
		rooms = new Room*[floor];
		fr.read((char*)room_cnt, sizeof(int)*floor);
		for(int i=0; i<floor; i++)
		{
			rooms[i] = new Room[room_cnt[i]];
			fr.read((char*)rooms[i], sizeof(Room)*room_cnt[i]);
		}
		fr.close();
	}
}

Hotel::~Hotel(void)
{
	delete[] room_cnt;
	delete[] rooms;
}
