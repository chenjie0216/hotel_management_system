#include "hotel_manage.h"

int h = 0, d = 0;

HotelManage::HotelManage(void)
{
	hotel = new Hotel;
}

void get_room_id(Hotel* hotel)
{
	int temp_room_id = 0;
	cout << "请输入房间号:";
	cin >> temp_room_id;
	clear_stdin();

	int h = temp_room_id / 100;
	int d = temp_room_id % 100;
	if(h > hotel->floor || d > hotel->room_cnt[h-1])
	{
		cout << "房间号输入有误,开房失败!" << endl;
		anykey_continue();
		return;
	}
}

void HotelManage::in(void)
{
	get_room_id(hotel);
	
	if(hotel->rooms[h-1][d-1].guests != NULL)
	{
		cout << "该房间未退房,开房失败!" << endl;
		anykey_continue();
		return;
	}
	
	int temp_num = 0;
	cout << "请输入入住人数:";
	cin >> temp_num;
	clear_stdin();

	if(temp_num < 1 || temp_num > hotel->rooms[h-1][d-1].cnt)
	{
		cout << "超过最大入住人数，开房失败!" << endl;
		anykey_continue();
		return;
	}
	
	hotel->rooms[h-1][d-1].guests = new Guest[temp_num];
	string temp_guest_name = "";
	int temp_guest_id = 0;
	for(int i=0; i<temp_num; i++)
	{
		cout << "房客" << i+1 << "姓名:";
		cin >> temp_guest_name;
		hotel->rooms[h-1][d-1].guests[i].set_name(temp_guest_name);
		cout << "房客" << i+1 << "身份证:";
		cin >> temp_guest_id;
		hotel->rooms[h-1][d-1].guests[i].set_id(temp_guest_id);
	}
	clear_stdin();
	cout << "开房成功" << endl;
	anykey_continue();
}

void HotelManage::out(void)
{
	get_room_id(hotel);
	
	if(hotel->rooms[h-1][d-1].guests == NULL)
	{
		cout << "该房间未使用,退房失败!" << endl;
		anykey_continue();
		return;
	}

	delete[] hotel->rooms[h-1][d-1].guests;
	hotel->rooms[h-1][d-1].guests = NULL;
	
	cout << "退房成功!" << endl;
	anykey_continue();
}

void HotelManage::show(void)
{
	for(int i=0; i<hotel->floor; i++)
	{
		for(int j=0; j<hotel->room_cnt[i]; j++)
		{
			cout << hotel->rooms[i][j].get_id() << " ";
			if(hotel->rooms[i][j].guests == NULL)
			{
				cout << "n" << endl;
			}
			else
			{
				cout << "y" << endl; 
			}
		}
	}
	anykey_continue();
}

void HotelManage::query(void)
{
	get_room_id(hotel);

	cout << "房间号:" << hotel->rooms[h-1][d-1].get_id() << endl;
	switch(hotel->rooms[h-1][d-1].get_type())
	{
		case Type1: cout << "类型:商务房" << endl; break;
		case Type2: cout << "类型:标准房" << endl; break;
		case Type3: cout << "类型:大床房" << endl; break;
		case Type4: cout << "类型:家庭房" << endl; break;
	}
	cout << "价格:" << hotel->rooms[h-1][d-1].get_price() << endl;
	cout << "房客上限:" << hotel->rooms[h-1][d-1].cnt << endl;
	anykey_continue();
}

void HotelManage::run(void)
{
	while(true)
	{
		clear_src();
		cout << "welcome to Gg Hotel" << endl;
		cout << "1.开房" << endl;
		cout << "2.退房" << endl;
		cout << "3.显示" << endl;
		cout << "4.查询" << endl;
		switch(get_cmd('1', '4'))
		{
			case '1': in(); break;
			case '2': out(); break;
			case '3': show(); break;
			case '4': query(); break;
		}
	}
}

HotelManage::~HotelManage(void)
{
	delete hotel;
}
