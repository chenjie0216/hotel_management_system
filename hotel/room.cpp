#include "room.h"

int Room::get_id(void)
{
	return id;
}

float Room::get_price(void)
{
	return price;
}

RoomType Room::get_type(void)
{
	return type;
}

void Room::set_type(RoomType type)
{
	this->type = type;
}

void Room::set_price(float price)
{
	this->price = price;
}

void Room::set_id(int id)
{
	this->id = id;
}
