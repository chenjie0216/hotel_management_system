#ifndef ROOM_H
#define ROOM_H

#include "guest.h"
#include "tools.h"

class Room
{
	RoomType type;
	float price;
	int id;
public:
	int cnt;
	Guest* guests;

	Room(void){ }
	Room(RoomType type, float price, uint32_t id):type(type), price(price), id(id){ }
	
	RoomType get_type(void);
	int get_id(void);
	float get_price(void);
	
	void set_type(RoomType type);
	void set_price(float price);
	void set_id(int id);
};

#endif//ROOM_H
