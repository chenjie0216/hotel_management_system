#include "tools.h"

// 从键盘获取指令
char get_cmd(char start, char end)
{
	if(start > end) return 0;
	
	cout << "请输入指令：" << endl;
	while(true)
	{
		char cmd = cin.get();
		if(cmd >= start && cmd <= end)
		{
			clear_stdin();
			return cmd;
		}
	}
}

//清理缓冲区
void clear_stdin(void)
{
	stdin->_IO_read_ptr = stdin->_IO_read_end;
}

// 任意键继续
void anykey_continue(void)
{
	puts("任意键继续...");
	getchar();
}

// 清理屏幕
void clear_src(void)
{
	system("clear");
}
