#ifndef TOOLS_H
#define TOOLS_H

#include <iostream>

using namespace std;

enum RoomType
{
	Type1 = 1,
	Type2 = 2,
	Type3 = 3,
	Type4 = 4,
};

// 获取指令
char get_cmd(char start, char end);
// 清理缓冲区
void clear_stdin(void);
// 任意键继续
void anykey_continue(void);
// 清理屏幕
void clear_src(void);

#endif//TOOLS_H
