#ifndef HOTEL_H
#define HOTEL_H

#include <fstream>
#include "room.h"

#define HOTEL_PATH "config/hotel.conf"

class Hotel
{
public:
	int floor;
	int* room_cnt;
	Room** rooms;

	Hotel(void);
	~Hotel(void);
};

#endif//HOTEL_H
