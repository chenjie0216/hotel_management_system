#include "guest.h"

string& Guest::get_name(void)
{
	return name;
}

int Guest::get_id(void)
{
	return id;
}

void Guest::set_name(string name)
{
	this->name = name;
}

void Guest::set_id(int id)
{
	this->id = id;
}
