#ifndef GUEST_H
#define GUEST_H

#include <iostream>
#include <cstdint>

using namespace std;

class Guest
{
	string name;
	int id;
public:
	Guest(void){}
	Guest(string name, int id):name(name), id(id){}

	string& get_name(void);
	int get_id(void);

	void set_name(string name);
	void set_id(int id);
};

#endif//GUEST_H
